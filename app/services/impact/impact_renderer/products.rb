require 'libxml'

module Impact
  class ImpactRenderer::Products
    include Spree::FrontendHelper

    def self.product_url(url_options, product)
      url = url_options[:host]
      url = url + ":" + url_options[:port].to_s if url_options[:port]
      url = url + "/products/" + product.slug
      url
    end

    def self.create_doc_xml(root, attributes=nil)
      doc = LibXML::XML::Document.new
      doc.encoding = LibXML::XML::Encoding::UTF_8
      doc.root = create_node(root, nil, attributes)
      doc
    end

    def self.create_node(name, value=nil, options=nil)
      node = LibXML::XML::Node.new(name)
      node.content = value.to_s unless value.nil?
      if options
        attributes = options.delete(:attributes)
        add_attributes(node, attributes) if attributes
      end
      node
    end

    def self.add_attributes(node, attributes)
      attributes.each do |name, value|
        LibXML::XML::Attr.new(node, name, value)
      end
    end

    def self.basic_product(url_options, current_store, current_currency, item, product)
      item << create_node("link", product_url(url_options, product))
      item << create_node("sku", product.sku)
      item << create_node("name", product.name)

      if product.respond_to?(:short_description) && product.short_description.present?
        item << create_node("description", product.short_description)
      elsif product.description.present?
        item << create_node("description", product.description)
      else
        item << create_node("description", product.meta_description)
      end

      if product.images&.count > 0
         item << create_node("image_link", product.images.first.my_cf_image_url(:large))
      end
      
      if product.respond_to?(:on_sale) && product.on_sale?
        item << create_node("price", sprintf("%.2f", product.original_price))
        item << create_node("original_price", sprintf("%.2f", product.price))
      else
        item << create_node("price", sprintf("%.2f", product.price))
        item << create_node("original_price", sprintf("%.2f", product.price))
      end

      item << create_node("currency", current_currency)
      item << create_node("availability", product.in_stock? ? "in stock" : "out of stock")
      item << create_node("category", find_first_deepest_category(product))
      item << create_node("condition", 'new')
      item << create_node("manufacturer", current_store.name)
    end

    def self.complex_product(url_options, current_store, current_currency, item, product, variant)
      options_xml_hash = Spree::Variants::XmlFeedOptionsPresenter.new(variant).xml_options
      
      item << create_node("link", product_url(url_options, product) + "?variant=" + variant.id.to_s)
      item << create_node("sku", variant.sku)
      item << create_node("name", product.name + ' ' + options_xml_hash.first.presentation)

      if product.respond_to?(:short_description) && product.short_description.present?
        item << create_node("description", product.short_description)
      elsif product.description.present?
        item << create_node("description", product.description)
      else
        item << create_node("description", product.meta_description)
      end
      
      all_images = product.images&.to_a + variant.images&.to_a
      if all_images.size > 0
        item << create_node("image_link", all_images.first.my_cf_image_url(:large))
      end
      
      if variant.respond_to?(:on_sale) && variant.on_sale?
        item << create_node("price", sprintf("%.2f", variant.original_price))
        item << create_node("original_price", sprintf("%.2f", variant.price))
      else
        item << create_node("price", sprintf("%.2f", variant.price))
        item << create_node("original_price", sprintf("%.2f", variant.price))
      end

      item << create_node("currency", current_currency)
      item << create_node("availability", product.in_stock? ? "in stock" : "out of stock")
      item << create_node("category", find_first_deepest_category(product))
      item << create_node("condition", 'new')
      item << create_node("manufacturer", current_store.name)
    end

    def self.xml(url_options, current_store, current_currency, products)
      doc = create_doc_xml("catalog")
      catalog = doc.root

      products = products.except(:limit, :offset)
      products.each_with_index do |product, index|
        if product.respond_to?(:is_in_hide_from_nav_taxon) && product.is_in_hide_from_nav_taxon? || !product.available?
          next
        else
          if product.variants_and_option_values(current_currency).any?
            product.variants.each do |variant|
              if variant.available?
                catalog << (item = create_node("product"))
                complex_product(url_options, current_store, current_currency, item, product, variant)
              end
            end
          else
            catalog << (item = create_node("product"))
            basic_product(url_options, current_store, current_currency, item, product)
          end
        end

        GC.start if index % 100 == 0 # forcing garbage collection
      end

      doc.to_s
    end
  end
end
